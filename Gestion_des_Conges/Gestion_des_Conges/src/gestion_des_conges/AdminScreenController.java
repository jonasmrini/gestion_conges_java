/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion_des_conges;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Mouad
 */
public class AdminScreenController implements Initializable {

    @FXML
    private Pane pane_1;
    @FXML
    private Pane pane_2;
    @FXML
    private Pane pane_3;
    @FXML
    private Pane pane_4;
    @FXML
    private Pane pane_5;
    @FXML
    private StackPane stackepane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void mouse_exit_1(MouseEvent event) {
        pane_1.setStyle("-fx-background-color: white; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_hover_1(MouseEvent event) {
        pane_1.setStyle("-fx-background-color: #377ce8; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_exit_2(MouseEvent event) {
        pane_2.setStyle("-fx-background-color: white; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_hover_2(MouseEvent event) {
        pane_2.setStyle("-fx-background-color: #377ce8; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_exit_3(MouseEvent event) {
        pane_3.setStyle("-fx-background-color: white; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_hover_3(MouseEvent event) {
        pane_3.setStyle("-fx-background-color: #377ce8; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_exit_4(MouseEvent event) {
        pane_4.setStyle("-fx-background-color: white; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_hover_4(MouseEvent event) {
        pane_4.setStyle("-fx-background-color: #377ce8; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_exit_5(MouseEvent event) {
        pane_5.setStyle("-fx-background-color: white; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void mouse_hover_5(MouseEvent event) {
        pane_5.setStyle("-fx-background-color: #377ce8; -fx-backgound-radius: 6px;");
    }

    @FXML
    private void homeScreen(MouseEvent event) {
        Stage home = new Stage();
        Parent root=null;
        
        try {
            root = FXMLLoader.load(getClass().getResource("HomeScreen.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Stage current = (Stage)pane_1.getScene().getWindow();
        Scene scene=new Scene(root);
        
        home.setScene(scene);
        home.initStyle(StageStyle.TRANSPARENT);
        
        current.hide();
        home.show();
    }

    @FXML
    private void employeesScreen(MouseEvent event) {
        Stage employees = new Stage();
        Parent root=null;
        
        try {
            root = FXMLLoader.load(getClass().getResource("EmployeesScreen.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Stage current = (Stage)pane_1.getScene().getWindow();
        Scene scene=new Scene(root);
        
        employees.setScene(scene);
        employees.initStyle(StageStyle.TRANSPARENT);
        
        current.hide();
        employees.show();
    }

    @FXML
    private void DemandeScreen(MouseEvent event) {
        Stage demande = new Stage();
        Parent root=null;
        
        try {
            root = FXMLLoader.load(getClass().getResource("DemandeScreen.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Stage current = (Stage)pane_1.getScene().getWindow();
        Scene scene=new Scene(root);
        
        demande.setScene(scene);
        demande.initStyle(StageStyle.TRANSPARENT);
        
        current.hide();
        demande.show();
    }

    @FXML
    private void exit(MouseEvent event) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("alerte"));
        dialogLayout.setBody(new Text("voulez-vous sortir ?"));
        
        JFXButton ok = new JFXButton("OK");
        JFXButton annuler = new JFXButton("ANNULER");
        
        JFXDialog dialog = new JFXDialog(stackepane, dialogLayout, JFXDialog.DialogTransition.CENTER);
        
        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    System.exit(0);
            }
        });
        
        annuler.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    dialog.close();
            }
        });
        
        
        dialogLayout.setActions(ok,annuler);
        dialog.show();
    }

    @FXML
    private void logout(MouseEvent event) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Se déconnecter"));
        dialogLayout.setBody(new Text("vous voulez vous déconnecter ?"));
        
        JFXButton ok = new JFXButton("OK");
        JFXButton annuler = new JFXButton("ANNULER");
        
        JFXDialog dialog = new JFXDialog(stackepane, dialogLayout, JFXDialog.DialogTransition.CENTER);
        
        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage demande = new Stage();
                Parent root=null;
        
        try {
                root = FXMLLoader.load(getClass().getResource("LoginScreen.fxml"));
        } catch (IOException ex) {
                Logger.getLogger(AdminScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                Stage current = (Stage)pane_1.getScene().getWindow();
                Scene scene=new Scene(root);
        
                demande.setScene(scene);
                demande.initStyle(StageStyle.TRANSPARENT);
        
                current.hide();
                demande.show();
                    
            }
        });
        
        annuler.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    dialog.close();
            }
        });
        
        
        dialogLayout.setActions(ok,annuler);
        dialog.show();
    }
    
}
