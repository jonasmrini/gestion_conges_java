/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employedao;

import Hibernate.Hibernate;
import employe.Employe;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;


public class EmployeDao {
	

	public void saveEmploye(Employe employe) {
		Transaction transaction = null;
		try (Session session = Hibernate.getSessionFactory().openSession()) {
		
			transaction = session.beginTransaction();
		
			session.save(employe);
		
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}


	public void updateEmploye(Employe employe) {
		Transaction transaction = null;
		try (Session session = Hibernate.getSessionFactory().openSession()) {
		
			transaction = session.beginTransaction();
		
			session.update(employe);
	
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}


	public void deleteEmploye(int id) {

		Transaction transaction = null;
		try (Session session = Hibernate.getSessionFactory().openSession()) {
			
			transaction = session.beginTransaction();

			
			Employe emlpoye = session.get(Employe.class, id);
			if (employe != null) {
				session.delete(employe);
				System.out.println("employe est supprimer");
			}

			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	

	public Employe getEmploye(int id) {

		Transaction transaction = null;
		Employe employe = null;
		try (Session session = Hibernate.getSessionFactory().openSession()) {
		
			transaction = session.beginTransaction();
		
			employe = session.get(Employe.class, id);
			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return employe;
	}
	

	@SuppressWarnings("unchecked")
	public List<Employe> getAllEmploye() {

		Transaction transaction = null;
		List<Employe> listOfEmploye = null;
		try (Session session = Hibernate.getSessionFactory().openSession()) {
		
			transaction = session.beginTransaction();
			
			
			listOfEmploye = session.createQuery("de la part de Employe").getResultList();
			
			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return listOfEmploye;
	}
}
