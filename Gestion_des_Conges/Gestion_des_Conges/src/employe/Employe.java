/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="instructor")

public class Employe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	protected int id;
	
	@Column(name="nom")
	protected String nom;
	
	@Column(name="adress")
	protected String adress;
	
	@Column(name="telephone")
	protected String telephone;
	
	public Employe() {
	}
	
	public Employe(String nom, String adress, String telephone) {
		super();
		this.nom = nom;
		this.adress = adress;
		this.telephone = telephone;
	}

	public Employe(int id, String nom, String adress, String telephone) {
		super();
		this.id = id;
		this.nom = nom;
		this.adress = adress;
		this.telephone = telephone;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
